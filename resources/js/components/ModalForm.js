import React, {Component} from 'react';

export class ModalForm extends Component {
  _toggleModal = () => {
    this.props.toggleModal();
  };

  _handleSubmit = (e) => {
    event.preventDefault();
    let form_data = new FormData(e.target);
    this.props.addNewItem(form_data);
    this.props.toggleModal();
  };

  render() {
    if (this.props.show) {
      return (
        <div className="modal" tabIndex="-1" role="dialog" style={{display: "block"}}>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <form onSubmit={this._handleSubmit}>
                <input type="hidden" id="id" name="id" defaultValue={this.props.preset.id}/>
                <div className="modal-header">
                  <h5 className="modal-title">Modal title</h5>
                  <button type="button" onClick={this._toggleModal} className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                    <div className="form-group">
                      <label htmlFor="word">Word</label>
                      <input type="text" className="form-control" id="signifier" name="signifier" defaultValue={this.props.preset.signifier}/>
                    </div>

                    <div className="form-group">
                      <label htmlFor="type">Type</label>
                      <input type="text" className="form-control" id="type" name="type" defaultValue={this.props.preset.type}/>
                    </div>

                    <div className="form-group">
                      <label htmlFor="meaning">Meaning</label>
                      <input type="text" className="form-control" id="meaning" name="meaning" defaultValue={this.props.preset.meaning}/>
                    </div>

                    <div className="form-group">
                      <label htmlFor="translation">Translation</label>
                      <input type="text" className="form-control" id="translation" name="translation" defaultValue={this.props.preset.translation}/>
                    </div>
                </div>
                <div className="modal-footer">
                  <button type="submit" className="btn btn-primary">Save changes</button>
                  <button type="button" onClick={this._toggleModal}  className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      )
    } else {
      return null;
    }
  }
}