import React, {Component} from 'react';

export class Pagination extends Component {
  _hasMorePages = () => { return this.props.page * 15 + 15 <= this.props.total_results; };

  render(){
    return (
        <div className="d-flex col-4 offset-4" >
          {this.props.page !== 0 && <button className="btn btn-primary mr-auto" onClick={this.props.prevPage} >Prev</button>}
          {this._hasMorePages() && <button className="btn btn-primary ml-auto" onClick={this.props.nextPage}>Next</button>}
        </div>
    );
  }
}
