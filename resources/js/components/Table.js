import React, {Component} from 'react';
import TableRowContainer from "../container/TableRow";

export class Table extends Component {
  state = {
    page: 0,
    highlighted: false
  };

  _fetchHighlighted = () => {
    if (!this.state.highlighted) {
      this.props.fetchHighlighted();
    } else {
      this.props.fetchWordsFromDatabase();
    }

    this.setState({highlighted: !this.state.highlighted})
  }

  componentDidMount() {
      this.props.fetchWordsFromDatabase();
  };

  render(){
    return (
      <div className="mt-4 row">
        <div className="col">
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>Word</th>
                  <th>Type</th>
                  <th>Meaning</th>
                  <th>Translations</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                {this.props.words.slice(this.props.page * 15, (this.props.page * 15) + 15).map(word => (
                  <TableRowContainer key={word.id} word={word} />
                ))}
              </tbody>
            </table>
          </div>
          <div className="d-flex justify-content-between">
            <small>{this.props.words.length} Results</small>
            <button className={`btn ${this.state.highlighted ? 'btn-info' : 'btn-light'}`} onClick={this._fetchHighlighted}>Highlighted</button>
          </div>        
        </div>
      </div>
    )
  }
}
