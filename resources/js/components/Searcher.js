import React, {Component} from 'react';

export class Searcher extends Component {
  _handleChange = (e) => {
    this.props.resetPagination();
    this.props.refreshFilterBy(e.target.value);
  };

  _handleClick = (e) => {
    e.target.value = '';
    this.props.refreshFilterBy('');
  };

  render(){
    return (
      <div className="row justify-content-sm-between">
        <div className="col-12 col-sm-6">
          <input type="field" className="form-control mw-300" onClick={this._handleClick} onChange={this._handleChange} placeholder="Search By..."/>
        </div>
        <div className="col-12 col-sm-6 text-right">
          <button onClick={this.props.toggleModalForm} className="btn btn-success btn-small mt-3 mt-sm-0">Add Word</button>
        </div>
      </div>
    )
  }
}

export default Searcher;