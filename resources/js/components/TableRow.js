import React, {Component} from 'react';

export class TableRow extends Component {
  _deleteWord = () => {
    this.props.deleteWord(this.props.word.id);
  };

  _editWord = () => {
    this.props.toggleModalForm();
    this.props.presetModalFormFields(this.props.word);
  };

  _highlightWord = () => {
    this.props.highlightWord(this.props.word.id);
  };

  render() {
    let word = this.props.word;
    return (
      <tr className={`${word.highlighted ? 'table-primary' : ''}`}>
        <td> {word.signifier} </td>
        <td> {word.type} </td>
        <td> {word.meaning} </td>
        <td> {word.translation} </td>
        <td className="text-center">
          <i style={{cursor: 'pointer'}} onClick={this._editWord} className="fa fa-pencil green p-1"></i>
          <i style={{cursor: 'pointer'}} onClick={this._deleteWord} className="fa fa-close red p-1"></i>
          <i style={{cursor: 'pointer'}} onClick={this._highlightWord} className={`fa fa-star p-1 ${!word.highlighted ? 'yellow' : ''}`}></i>
        </td>
      </tr>
    )
  }


}