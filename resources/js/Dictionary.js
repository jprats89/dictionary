import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import TableContainer from './container/Table';
import SearcherContainer from './container/Searcher';
import ModalFormContainer from './container/ModalForm';
import Pagination from './components/Pagination';
import {appReducer} from './reducers';
import PaginationContainer
  from "./container/Pagination";

const store = createStore(appReducer, applyMiddleware(thunk));

class Dictionary extends Component {
  state = {
    page: 0
  };

  _nextPage = () => this.setState({ page: this.state.page + 1 });
  _prevPage = () => this.setState({ page: this.state.page - 1 });
  _resetPagination = () => this.setState({ page: 0 });

  render(){
    return (
      <Provider store={store}>
        <ModalFormContainer />
        <div className="card-header">
          <SearcherContainer resetPagination={this._resetPagination}/>
        </div>
        <div className="card-body">
          <TableContainer page={this.state.page}/>
          <PaginationContainer page={this.state.page} prevPage={this._prevPage} nextPage={this._nextPage}/>
        </div>
      </Provider>
    );
  }
}

export default Dictionary;
ReactDOM.render(<Dictionary />, document.getElementById('dictionary'));

