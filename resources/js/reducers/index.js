const INITIAL_STATE = {
  words: [],
  matching_words: [],
  total_matching_words: 0,
  filter_by: '',
  show_modal_form: false,
  modal_form_fields_preset: {}
};

export function appReducer (state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'REFRESH_FILTER_BY':
      return state = Object.assign({}, state, {
        filter_by: action.filter_by_input,
        modal_form_fields_preset: {signifier: action.filter_by_input},
        matching_words: state.words.filter( word => word.signifier.toUpperCase().includes(action.filter_by_input.toUpperCase()))
      });

    case 'TOGGLE_MODAL_FORM':
      return Object.assign({}, state, {
        modal_form_fields_preset: {signifier: state.filter_by},
        show_modal_form: !state.show_modal_form,
      });

    case 'FETCH_WORDS_FROM_DATABASE':
      return {
        ...state,
        words: action.words,
        matching_words: (state.filter_by === '')
          ? action.words
          : action.words.filter( word => word.signifier.toUpperCase().includes(state.filter_by.toUpperCase()))
      };

    case 'PRESET_MODAL_FORM_FIELDS':
      return {
        ...state,
        modal_form_fields_preset: action.word
      };

    default:
      return state;
  }
}
