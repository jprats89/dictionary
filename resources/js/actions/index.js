import axios from "axios";

export const refreshFilterByAction = filter_by_input => ({
  type: 'REFRESH_FILTER_BY',
  filter_by_input
});

export const toggleModalFormAction = () => ({
  type: 'TOGGLE_MODAL_FORM',
});

export const fetchWordsFromDatabaseAction = words => ({
  type: 'FETCH_WORDS_FROM_DATABASE',
  words
});


export const presetModalFormFieldsAction = word => ({
  type: 'PRESET_MODAL_FORM_FIELDS',
  word
});

export const deleteWordAction = (word_id) => {
  return function(dispatch) {
    thunkedDeleteWordFromDatabaseAction(word_id).then(function () {
      dispatch(thunkedFetchWordsFromDatabaseAction());
    });
  }
};

export const thunkedDeleteWordFromDatabaseAction = (word_id) => {
  return axios.delete('api/dictionary/'+word_id);
};

export const addNewItemAction = form_data => {
  return function(dispatch) {
    thunkedInsertNewItemInDatabaseAction(form_data).then(function () {
      dispatch(thunkedFetchWordsFromDatabaseAction());
    });
  }
};

export const thunkedInsertNewItemInDatabaseAction = (form_data) => {
  return axios.post('api/dictionary', {
    id: form_data.get('id'),
    signifier: form_data.get('signifier'),
    type: form_data.get('type'),
    meaning: form_data.get('meaning'),
    translation: form_data.get('translation')
  });
};

export const highlightWordAction = (word_id) => {
  return function(dispatch) {
    thunkedHighlightWordAction(word_id).then(function () {
      dispatch(thunkedFetchWordsFromDatabaseAction());
    });
  }
};

export const thunkedHighlightWordAction = (word_id) => {
  return axios.put('api/dictionary/highlight/'+word_id);
};


export const thunkedFetchWordsFromDatabaseAction = () => {
  return function(dispatch) {
    return axios.get('api/dictionary/list')
      .then(function (resp) {
        dispatch(fetchWordsFromDatabaseAction(resp.data));
      });
  }
};

export const thunkedFetchHiglightedWordsAction = () => {
  return function(dispatch) {
    return axios.get('api/dictionary/highlighted')
      .then(function (resp) {
        dispatch(fetchWordsFromDatabaseAction(resp.data));
      });
  }
};
