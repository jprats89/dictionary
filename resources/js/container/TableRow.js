import {connect} from 'react-redux';
import {TableRow} from "../components/TableRow";
import {
  deleteWordAction,
  presetModalFormFieldsAction,
  toggleModalFormAction,
  highlightWordAction
} from "../actions";

const mapDispatchToProps = dispatch => ({
  deleteWord: word_id => dispatch(deleteWordAction(word_id)),
  presetModalFormFields: word => dispatch(presetModalFormFieldsAction(word)),
  toggleModalForm: () => dispatch(toggleModalFormAction()),
  highlightWord: word_id => dispatch(highlightWordAction(word_id))
});

const createConnection = connect(null, mapDispatchToProps);
const TableRowContainer = createConnection(TableRow);
export default TableRowContainer;
