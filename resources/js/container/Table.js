import {connect} from 'react-redux';
import {Table} from "../components/Table";
import {thunkedFetchWordsFromDatabaseAction, thunkedFetchHiglightedWordsAction} from '../actions';

const mapStateToProps = (state) => {
  return {words: state.matching_words}
};

const mapDispatchToProps = dispatch => ({
  fetchWordsFromDatabase: () => dispatch(thunkedFetchWordsFromDatabaseAction()),
  fetchHighlighted: () => dispatch(thunkedFetchHiglightedWordsAction())
});

const createConnection = connect(mapStateToProps, mapDispatchToProps);
const TableContainer = createConnection(Table);

export default TableContainer;
