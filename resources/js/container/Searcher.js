import {connect} from 'react-redux';
import {Searcher} from "../components/Searcher";
import { refreshFilterByAction, toggleModalFormAction } from '../actions'

const mapDispatchToProps = dispatch => ({
  refreshFilterBy: filter_by_input => dispatch(refreshFilterByAction(filter_by_input)),
  toggleModalForm: () => dispatch(toggleModalFormAction())
});

const createConnection = connect(null, mapDispatchToProps);
const SearcherContainer = createConnection(Searcher);
export default SearcherContainer;
