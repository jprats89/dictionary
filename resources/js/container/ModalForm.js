import {connect} from 'react-redux';
import {ModalForm} from "../components/ModalForm";
import {toggleModalFormAction, addNewItemAction} from "../actions";

const mapStateToProps = (state) => {
  return {
    show: state.show_modal_form,
    preset: state.modal_form_fields_preset
  }
};

const mapDispatchToProps = dispatch => ({
  toggleModal: () => dispatch(toggleModalFormAction()),
  addNewItem: form_data => dispatch(addNewItemAction(form_data)),
});

const createConnection = connect(mapStateToProps, mapDispatchToProps);
const ModalFormContainer = createConnection(ModalForm);
export default ModalFormContainer;
