import {connect} from 'react-redux';
import {Pagination} from "../components/Pagination";

const mapStateToProps = (state) => {
  return {total_results: state.matching_words.length}
};

const mapDispatchToProps = () => ({

});

const createConnection = connect(mapStateToProps, mapDispatchToProps);
const PaginationContainer = createConnection(Pagination);
export default PaginationContainer;
