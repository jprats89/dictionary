<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('dictionary')->group(function () {
    Route::get('/list', array('uses' => 'DictionaryController@index'));
    Route::get('/highlighted', array('uses' => 'DictionaryController@getHighlighted'));
    Route::post('/', array('uses' => 'DictionaryController@store'));
    Route::put('/highlight/{word}', array('uses' => 'DictionaryController@highlight'));
    Route::delete('/{id}', array('uses' => 'DictionaryController@destroy'));
});
