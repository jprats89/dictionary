<?php

namespace App;

use App\Traits\UcfirstTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    use UcfirstTrait;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('signifier', 'asc');
        });
    }

    protected $table = 'dictionary';
    protected $fillable = ['signifier', 'type', 'meaning', 'translation'];
}
