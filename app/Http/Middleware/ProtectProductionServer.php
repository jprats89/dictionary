<?php

namespace App\Http\Middleware;

use Closure;

class ProtectProductionServer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_ENV') == 'prod') {
            return app(Authenticate::class)->handle($request, function ($request) use ($next) {
                return $next($request);
            });
        }

        return $next($request);
    }
}
