<?php

namespace App\Http\Controllers;

use App\Dictionary;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    public function index(){
        return Dictionary::all();
    }

    public function store(Request $request)
    {
        Dictionary::updateOrCreate(['id' => $request->get('id')], $request->all());
    }

    public function destroy($id_word)
    {
        Dictionary::destroy($id_word);
    }

    public function getHighlighted(){
        return Dictionary::where('highlighted', true)->inRandomOrder()->get();
    }
    
    public function highlight(Dictionary $word)
    {
        $word->highlighted = !$word->highlighted;
        $word->save();
    }
}
