#### Description
Dictionary is a side project implemented with <b>Laravel + React + Redux + Axios</b> in which one will be able to create a set of words. These words will be composed of signifier, type, meaning and translation.
The list of words will be shown in a table which allows filtering.

Te main aim of this project is to practice the communications between Frontend and Backend frameworks through and API.

#### Demo site
Fancy to check the [demo version](http://ec2-3-248-156-23.eu-west-1.compute.amazonaws.com/dictionary_test/public/)?




