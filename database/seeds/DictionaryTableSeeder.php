<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use League\Csv\Reader;

class DictionaryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(storage_path('app/public/file.csv'), 'r');
        $records = $csv->getRecords();
        foreach ($records as $offset => $record) {
            DB::table('dictionary')->insert([
                [
                    'signifier' => $record[0],
                    'type' => $record[1],
                    'meaning' => $record[2],
                    'translation' => $record[3],
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]
            ]);
        }
    }
}
